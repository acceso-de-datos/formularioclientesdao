package vista;

import controladorbd.ClienteBD;
import dao.ClienteDAO;

import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import modelo.Cliente;

public class ControllerCliente implements Initializable {

    private final static int MODO_NAVEGACION = 0;
    private final static int MODO_NUEVO_REGISTRO = 1;
    private final static int MODO_EDITA_REGISTRO = 2;

    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnBorrarOAceptar;
    @FXML
    private Button btnEditarOCancelar;
    @FXML
    private TextField tfID;
    @FXML
    private TextField tfNombre;
    @FXML
    private TextField tfDireccion;
    @FXML
    private TextField tfIDBuscar;
    @FXML
    private Label lblInfo;
    
private ClienteDAO cliDAO;
    private ClienteBD clibd;
    private Cliente cli;
    private int modo;
    private List<Cliente>listClientes;
	private int posicion;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        tfID.setDisable(true);
        try {
        	cliDAO = new ClienteDAO();
        	listClientes = cliDAO.findAll();
        	posicion =0;
            clibd = new ClienteBD();
            cli = clibd.primero();
            mostrarRegistro();
        } catch (SQLException ex) {
        	Util.mensajeExcepcion(ex, "Conectando con la base de datos...");
            Platform.exit();
        }

    }

    // ******************************************************************************
    // ACCIONES ASOCIADAS A BOTONES
    // ******************************************************************************
    @FXML
    private void accionPrimero() {
    	cli = listClientes.get(0);
    	posicion =0;
    	mostrarRegistro();
    }

    @FXML
    private void accionAtras() {
    	if(posicion>=1) {
    		posicion--;
    		cli=listClientes.get(posicion);
    		mostrarRegistro();
    	}
      
    }

    @FXML
    private void accionAdelante() {
    	if(posicion<listClientes.size()-1) {
    		posicion++;
    		cli=listClientes.get(posicion);
    		mostrarRegistro();
    	}
    	
     
    }

    @FXML
    private void accionUltimo() {
    	cli = listClientes.get(listClientes.size()-1);
    	posicion =listClientes.size()-1;
    	mostrarRegistro();
       
    }

    @FXML
    private void accionNuevo() {
        modo = MODO_NUEVO_REGISTRO;
        cambiarModo();

    }

    @FXML
    private void accionEditarOCancelar() {
        if (modo == MODO_NAVEGACION) {          // accion editar
            try {
                if (clibd.totalRegistros() > 0) {
                    modo = MODO_EDITA_REGISTRO;
                    cambiarModo();
                }
            } catch (SQLException ex) {
            	Util.mensajeExcepcion(ex, "Editando registro...");
            }
        } else {                                // accion cancelar//            
            mostrarRegistro();
            modo = MODO_NAVEGACION;
            cambiarModo();
        }

    }
    
    @FXML
    void accionBuscarID(ActionEvent event) throws NumberFormatException, SQLException {
    	if(tfIDBuscar.getText()!="") {
    		if(isNumeric(tfIDBuscar.getText())) {
    	
    			
    		cli = cliDAO.findByPK(Integer.parseInt(tfIDBuscar.getText()));
        		mostrarRegistro();
    		}
    		
    	}
   
    
    }

    @FXML
    private void accionBorrarOAceptar() throws SQLException {
        if (modo == MODO_NAVEGACION) {  
        	
        	if(listClientes.size()>0) {
        		 String mensaje = "¿Estás seguro de borrar el registro [" + tfID.getText() + "]?";
                 Alert d = new Alert(Alert.AlertType.CONFIRMATION, mensaje, ButtonType.YES, ButtonType.NO);
                 d.setTitle("Borrado de registro");
                 d.showAndWait();
                 if (d.getResult() == ButtonType.YES) {
                	 cliDAO.delete(listClientes.get(posicion));
                	 if(listClientes.size()==0) {
                		 cli=null;
                	 }else {
                		 listClientes.remove(posicion);
                		 if(posicion>listClientes.size()-1) {
                			 posicion=listClientes.size()-1;
                			 cli= listClientes.get(posicion);
                		 }
                		 
                	 }
                	 mostrarRegistro();
                 }
        	}
        	
        	
        } else {                            // accion aceptar
            try {
                Cliente c = new Cliente(tfNombre.getText(), tfDireccion.getText());
                if (modo == MODO_NUEVO_REGISTRO) {
                	if(listClientes.add(cliDAO.insertGenKey(c))) {
                    cli = listClientes.get(listClientes.size()-1);
                    posicion=listClientes.size()-1;
                	}
                } else {
                	if(cliDAO.update(c))
                    cli = listClientes.set(posicion,cli);
                }
                mostrarRegistro();
                modo = MODO_NAVEGACION;
                cambiarModo();
            } catch (SQLException ex) {
            	Util.mensajeExcepcion(ex, "Actualizando registro...");
            }

        }
    }

    private void mostrarRegistro() {
        try {
            lblInfo.setText("Registro " + clibd.registroActual() + " de " + clibd.totalRegistros());
        } catch (SQLException ex) {
            Util.mensajeExcepcion(ex, "Mostrando registro...");
        }
        if (cli != null) {
            tfID.setText(String.valueOf(cli.getId()));
            tfNombre.setText(cli.getNombre());
            tfDireccion.setText(cli.getDireccion());
        } else {
            tfID.setText(String.valueOf(0));
            tfNombre.setText("");
            tfDireccion.setText("");
        }
    }

    private void cambiarModo() {
        switch (modo) {
            case MODO_NAVEGACION:
                btnNuevo.setDisable(false);
                btnBorrarOAceptar.setText("Borrar");
                btnEditarOCancelar.setText("Editar");
                tfNombre.setEditable(false);
                tfDireccion.setEditable(false);
                break;
            case MODO_NUEVO_REGISTRO:
                tfID.setText("<autonum>");
                tfNombre.setText("");
                tfDireccion.setText("");
            case MODO_EDITA_REGISTRO:
                btnNuevo.setDisable(true);
                btnBorrarOAceptar.setText("Aceptar");
                btnEditarOCancelar.setText("Cancelar");
                tfNombre.setEditable(true);
                tfDireccion.setEditable(true);
                tfNombre.requestFocus();

        }

    }
    
    private static boolean isNumeric(String cadena){
    	try {
    		Integer.parseInt(cadena);
    		return true;
    	} catch (NumberFormatException nfe){
    		return false;
    	}
    }

   
}
